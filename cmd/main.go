package main

import (
	"example/marc/project-zero/pkg/feat"
	"fmt"
	"sync"
)

func main() {
	defer fmt.Println("Main ended.")
	fmt.Println("Main started...")

	// WaitGroup is used to wait for the program to finish goroutines.
	var wg sync.WaitGroup

	inputChan := make(chan int)

	// Add one for each goroutine.
	wg.Add(1)
	go func() {
		feat.Run(inputChan)
		// Schedule the call to WaitGroup's Done to tell goroutine is completed.
		wg.Done()
	}()

	for i := 0; i < 10; i++ {
		inputChan <- i
	}
	close(inputChan)

	// Wait for the goroutines to finish.
	wg.Wait()
}
