package feat

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func double(i int) int {
	return i * 2
}

func TestDouble(t *testing.T) {
	// GIVEN input arg
	i := 2
	// WHEN run func on input arg
	j := double(i)
	// THEN func output equals expected hardcoded output
	assert.Equal(t, 4, j)
}
