package feat

import (
	"fmt"
)

func Run(ch <-chan int) {
	defer fmt.Println("Runner ended.")
	fmt.Println("Runner started..")

	for i := range ch {
		fmt.Printf("Runner received %d\n", i)
	}
}
