# Notes:

`go mod init module_name` inits the go.mod file and names the module module_name.
`module_name` should look like `company/user/project` e.g. `example/marc/project-zero`.

`go install example/marc/project-zero`
builds the `project-zero` command, producing an executable binary.
It then installs that binary as $HOME/go/bin/project-zero.
`example/marc/project-zero` is the module name in go.mod.